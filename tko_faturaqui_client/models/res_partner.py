from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from . import at_constants


class Partner(models.Model):
    _inherit = 'res.partner'

    at_self_billing_indicator = fields.Selection(at_constants.at_self_billing_indicators,
                                                 'AT Self Billing Indicator', required=True,
                                                 company_dependent=True,
                                                 help=_('Indicator of the existence of a self-billing agreement '
                                                        'between the customer and the supplier.'))

    @api.onchange('vat')
    def _onchange_vat(self):
        if self._origin.total_invoiced:
            if self._origin.vat and self._origin.vat != 'PT999999990':
                raise ValidationError(_('Cannot change VAT for already invoiced partner'))
